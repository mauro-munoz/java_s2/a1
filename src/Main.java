import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scannerName = new Scanner (System.in);

        String[] newArray = new String[5];
        newArray[0] = "apple";
        newArray[1] = "avocado";
        newArray[2] = "banana";
        newArray[3] = "kiwi";
        newArray[4] = "orange";
        System.out.println("Fruits in Stock: "+ Arrays.toString(newArray));
        System.out.println("Which fruit would you like to get the index of?");
        String fruit = scannerName.nextLine();
        int result = Arrays.binarySearch(newArray,fruit);
        System.out.println("The index of "+ fruit + " is "+result);

        ArrayList<String> friends = new ArrayList<>(Arrays.asList("Zoro","Sanji","Chopper","Usopp"));
        System.out.println("My friends are: " + friends);

        HashMap<String,Integer> items = new HashMap<>();
        items.put("ManaPotion",23);
        items.put("HealthPotion",3);
        items.put("Arrows",99);
        System.out.println("Our current inventory consists of: " + items);
    }
}